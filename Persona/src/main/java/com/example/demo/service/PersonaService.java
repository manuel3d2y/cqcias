/**
 * 
 */
package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IPersonaDao;
import com.example.demo.model.PersonaModel;

/**
 * capa de sevivio, imprementacion
 * @author josel
 *
 */
@Service
public class PersonaService implements IPersonaService{
	
	/**
	 * Variable para el acceso a datos
	 */
	@Autowired
	private IPersonaDao dao;
	
	/**
	 *Consulta oersonas activas
	 */
	@Override
	public List<PersonaModel> ConsultaPersonas() {
		List<PersonaModel> lstActivos = new ArrayList<>();
		List<PersonaModel> all = dao.ConsultaPersonas();
		
		//Agrega a la lista los estatus activos
		for (PersonaModel row : all) {
			if(row.getEstatus().equals("1")) {
				lstActivos.add(row);
			}
		}
		
		return lstActivos;
	}

	/**
	 *Ontine el detalle de la persona seleccionada
	 */
	@Override
	public PersonaModel obtenerDetalle(int id) {
		Optional<PersonaModel> optional = dao.obtenerDetalle(id);
		
		PersonaModel detalle = new PersonaModel();
		detalle.setId(optional.get().getId());
		detalle.setNombre(optional.get().getNombre());
		detalle.setPrimerApellido(optional.get().getPrimerApellido());
		detalle.setSegundoApellido(optional.get().getSegundoApellido());
		detalle.setTelefono(optional.get().getTelefono());
		if("1".equals(optional.get().getEstatus())) {
			detalle.setEstatus("Activo");
		} else if("0".equals(optional.get().getEstatus())){
			detalle.setEstatus("Inactivo");
		}
		return detalle;
	}

	/**
	 * Guarda la persona
	 */
	@Override
	public PersonaModel guardar(PersonaModel persona) {
		PersonaModel p = dao.guardar(persona);
		return p;
	}

}
