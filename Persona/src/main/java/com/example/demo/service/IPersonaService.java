/**
 * 
 */
package com.example.demo.service;

import java.util.List;

import com.example.demo.model.PersonaModel;

/**
 * interfaz de servicio persona
 * @author josel
 *
 */
public interface IPersonaService {
	
	/**
	 * Consulta personas activas
	 * @return List<PersonaModel> listado de personas activas
	 */
	public List<PersonaModel> ConsultaPersonas();

	
	/**
	 * Obtiene los datos de la persona seleccionada
	 * @param id identificador de llave primaria
	 * @return
	 */
	public PersonaModel obtenerDetalle(int id);

	/**
	 * Guarda una nueva perona
	 * @param persona modelo de la persona
	 * @return PersonaModel modelo de persona
	 */
	public PersonaModel guardar(PersonaModel persona);
}
