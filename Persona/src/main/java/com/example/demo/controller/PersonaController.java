/**
 *  clase que gestiona las persona
 */
package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.model.PersonaModel;
import com.example.demo.service.IPersonaService;

/**
 *
 * @author josel
 * 
 *
 */
@Controller
@RequestMapping
public class PersonaController {
	
	@Autowired
	private IPersonaService service;
	
	/**
	 * metodo que consulta las personas activas
	 * @param model modelo
	 * @return pagina principal
	 */
	@GetMapping("/consulta")
	public String Consultar (Model model) {
		List<PersonaModel> lstPersonas = service.ConsultaPersonas();
		model.addAttribute("lstPersonas",lstPersonas);
		return "inicio";
	}
	
	/**
	 * Metodo que obtiene el detalle
	 * @param model modelo
	 * @param id identificador
	 * @return pagina principal
	 */
	@GetMapping("/detalle/{id}")
	public String obtenerDetalle (Model model, @PathVariable int id) {
		PersonaModel detalle = service.obtenerDetalle(id);
		model.addAttribute("detallePersona",detalle);
		System.out.println(detalle.toString());
		return "inicio";
	}
	
	/**
	 * Este metodo permite guardar una persona	
	 * @param persona modelo
	 * @param model modelo
	 * @return redirige al revivio de consulta
	 */
	@PostMapping("/guardar")
	@RequestMapping(value = "/guardar", method = RequestMethod.POST) 
	public String guardar(@ModelAttribute PersonaModel persona,  Model model) {
		//Guarda los datos de la persona
		service.guardar(persona);
		//REdirecciona al metodo de consultar
		return "redirect:/consulta";
	}

}
