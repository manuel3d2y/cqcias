/**
 * 
 */
package com.example.demo.dao;

import java.util.List;
import java.util.Optional;

import com.example.demo.model.PersonaModel;

/**
 * Interfaz de acceso a datos para la gestion de personas
 * @author josel
 *
 */
public interface IPersonaDao {
	
	
	/**
	 * Consulta las personas activas
	 * @return List<PersonaModel> lista de personas
	 */
	public List<PersonaModel> ConsultaPersonas();

	/**
	 * Obtine el detalle de la persona
	 * @param id identificador
	 * @return Optional<PersonaModel> datos de la persona
	 */
	public Optional<PersonaModel>  obtenerDetalle(int id);

	/**
	 * Guarda una persona
	 * @param persona modelo
	 * @return PersonaModel modelo de la persona  guardada
	 */
	public PersonaModel guardar(PersonaModel persona);
}
