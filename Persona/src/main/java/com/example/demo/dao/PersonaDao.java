/**
 * 
 */
package com.example.demo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.Repository.PersonaRepository;
import com.example.demo.model.PersonaModel;

/**
 * @author josel
 *
 */
@Repository
public class PersonaDao implements IPersonaDao {

	/**
	 * Variable de reposotory
	 */
	@Autowired
	private PersonaRepository reposytory;
	
	/**
	 * Consulta todas la personas
	 * @return
	 */
	public List<PersonaModel> ConsultaPersonas() {
		return (List<PersonaModel>) reposytory.findAll();
	}

	/**
	 *Obtiene el detalle de las personas
	 */
	public Optional<PersonaModel> obtenerDetalle(int id) {
		return reposytory.findById(id);
	}

	/**
	 * Guarda una nueva persona
	 */
	public PersonaModel guardar(PersonaModel persona) {
		PersonaModel p = reposytory.save(persona);
		return p;
	}
	
}
