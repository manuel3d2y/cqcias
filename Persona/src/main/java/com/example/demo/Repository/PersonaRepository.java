
/**
 * 
 */
package com.example.demo.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.PersonaModel;

/**
 * @author jose
 *
 */
@Repository
public interface PersonaRepository extends CrudRepository<PersonaModel, Integer>{

}
