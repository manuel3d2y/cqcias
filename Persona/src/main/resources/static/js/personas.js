
//se inicializa el script
$( document ).ready(function() {
    console.log( "ready!" );
    var str = window.location.href;
  	var isDetalle = str.includes("/detalle");
  	
  	//si está en la ruta de detalle, entonces prepara el modal y lo muestra
  	if(isDetalle){
  		$("#tituloModal").text("Detalle persona");
		$("#btnAgregar").hide();
		
		//muestra tabla de detalle
		$("#formAgregar").hide();
		$("#tblDetalle").show();
		$("#modalDetalle").modal("show");
  	}
});

//Abre el modal con el detalle de la perona seleccionada
function verDetalle(id){
	console.log("estas viendo el detalle de id: "+id);
	
	$("#actionDetalle").attr("href", "/detalle/"+id);
	window.location = $("#actionDetalle").attr("href");
	//$("#actionDetalle").trigger("click");
}

//cierra el modal y vielve a la pagina principal
function cerrar(){
	window.location = $("#btnCerrar").attr("href");
}
